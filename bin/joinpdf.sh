#!/bin/sh

dist_dir=public
pdf_dir=${dist_dir}/pdf

mkdir -p ${dist_dir}

pdf_files="\
  ${pdf_dir}/general/aerodrome-markings/index.pdf\
  ${pdf_dir}/general/aircraft-equipment/index.pdf\
  ${pdf_dir}/general/conversions/index.pdf\
  ${pdf_dir}/general/general/index.pdf\
  ${pdf_dir}/general/licensing/index.pdf\
  ${pdf_dir}/general/pilot-responsibilities/index.pdf\
  ${pdf_dir}/general/radar-transponders/index.pdf\
  ${pdf_dir}/general/radio-telephony-procedures/index.pdf\
  ${pdf_dir}/pre-flight-planning/designated-remote-areas/index.pdf\
  ${pdf_dir}/pre-flight-planning/flight-information-service/index.pdf\
  ${pdf_dir}/pre-flight-planning/flights-over-water/index.pdf\
  ${pdf_dir}/pre-flight-planning/meteorology/index.pdf\
  ${pdf_dir}/pre-flight-planning/pre-flight-briefing-and-notification/index.pdf\
  ${pdf_dir}/pre-flight-planning/preparation/index.pdf\
  ${pdf_dir}/pre-flight-planning/safety-precautions/index.pdf\
  ${pdf_dir}/operations/aerial-sporting-and-recreational-activities/index.pdf\
  ${pdf_dir}/operations/air-defence-identification-zone/index.pdf\
  ${pdf_dir}/operations/class-d-airspace/index.pdf\
  ${pdf_dir}/operations/class-e-and-class-g-airspace/index.pdf\
  ${pdf_dir}/operations/controlled-airspace/index.pdf\
  ${pdf_dir}/operations/cruising-level-requirements/index.pdf\
  ${pdf_dir}/operations/general-information/index.pdf\
  ${pdf_dir}/operations/night-vfr.pdf\
  ${pdf_dir}/operations/non-controlled-aerodromes/index.pdf\
  ${pdf_dir}/helicopter-operations/procedures/index.pdf\
  ${pdf_dir}/helicopter-operations/requirements/index.pdf\
  ${pdf_dir}/emergency-procedures/communication-failure/index.pdf\
  ${pdf_dir}/emergency-procedures/distress-beacons/index.pdf\
  ${pdf_dir}/emergency-procedures/emergency-signals/index.pdf\
  ${pdf_dir}/emergency-procedures/forced-landings/index.pdf\
  ${pdf_dir}/emergency-procedures/mercy-flights/index.pdf\
  ${pdf_dir}/emergency-procedures/planning/index.pdf\
  ${pdf_dir}/emergency-procedures/quick-reference.pdf\
  ${pdf_dir}/resources/abbreviations-and-acronyms/index.pdf\
  ${pdf_dir}/resources/definitions/index.pdf\
  ${pdf_dir}/resources/images/1/index.pdf\
  ${pdf_dir}/resources/images/2/index.pdf\
  ${pdf_dir}/resources/images/3/index.pdf\
  ${pdf_dir}/resources/images/4/index.pdf\
  ${pdf_dir}/resources/images/5/index.pdf\
  ${pdf_dir}/resources/images/6/index.pdf\
  ${pdf_dir}/resources/images/7/index.pdf\
  ${pdf_dir}/resources/images/8/index.pdf\
  ${pdf_dir}/resources/quick-reference/index.pdf\
  ${pdf_dir}/resources/section-general/1/index.pdf\
  ${pdf_dir}/resources/section-general/2/index.pdf\
  ${pdf_dir}/resources/section-operations/1/index.pdf\
  ${pdf_dir}/resources/section-operations/2/index.pdf\
  ${pdf_dir}/resources/section-operations/3/index.pdf\
  ${pdf_dir}/resources/section-operations/4/index.pdf\
  ${pdf_dir}/resources/section-operations/5/index.pdf\
  ${pdf_dir}/resources/section-emergency/index.pdf\
  ${pdf_dir}/resources/section-helicopter/index.pdf\
  ${pdf_dir}/resources/section-planning/1/index.pdf\
  ${pdf_dir}/resources/section-planning/2/index.pdf\
  ${pdf_dir}/resources/section-planning/3/index.pdf\
  ${pdf_dir}/resources/videos/index.pdf\
  "

pdfunite ${pdf_files} ${pdf_dir}/vfrg.casa.gov.au.pdf
