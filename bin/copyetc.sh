#!/bin/sh

dist_dir=public
etc_dir=etc

mkdir -p ${dist_dir}

rsync -aH ${etc_dir}/ ${dist_dir}
