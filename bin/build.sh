#!/bin/sh

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

${SCRIPTPATH}/scrape.sh
${SCRIPTPATH}/makepdf.sh
${SCRIPTPATH}/copyetc.sh
${SCRIPTPATH}/joinpdf.sh
