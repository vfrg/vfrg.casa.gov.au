#!/bin/sh

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

dist_dir=public
html_dir=${dist_dir}/vfrg.casa.gov.au
pdf_dir=${dist_dir}/pdf

makepdf_cmd="prince"
makepdf_options="--pdf-creator=TonyMorris --page-size=A4"
makepdf="${makepdf_cmd} ${makepdf_options}"

mkdir -p ${pdf_dir}

# general

aerodrome_markings=general/aerodrome-markings
mkdir -p ${pdf_dir}/${aerodrome_markings}
${makepdf} --pdf-title="Aerodrome Markings" ${html_dir}/${aerodrome_markings}/index.html -o ${pdf_dir}/${aerodrome_markings}/index.pdf

aircraft_equipment=general/aircraft-equipment
mkdir -p ${pdf_dir}/${aircraft_equipment}
${makepdf} --pdf-title "Aircraft Equipment" ${html_dir}/${aircraft_equipment}/index.html -o ${pdf_dir}/${aircraft_equipment}/index.pdf

conversions=general/conversions
mkdir -p ${pdf_dir}/${conversions}
${makepdf} --pdf-title "Conversions" ${html_dir}/${conversions}/index.html -o ${pdf_dir}/${conversions}/index.pdf

general=general/general
mkdir -p ${pdf_dir}/${general}
${makepdf} --pdf-title "General" ${html_dir}/${general}/index.html -o ${pdf_dir}/${general}/index.pdf

licensing=general/licensing
mkdir -p ${pdf_dir}/${licensing}
${makepdf} --pdf-title "Licensing" ${html_dir}/${licensing}/index.html -o ${pdf_dir}/${licensing}/index.pdf

pilot_responsibilities=general/pilot-responsibilities
mkdir -p ${pdf_dir}/${pilot_responsibilities}
${makepdf} --pdf-title "Pilot Responsibilities" ${html_dir}/${pilot_responsibilities}/index.html -o ${pdf_dir}/${pilot_responsibilities}/index.pdf

radar_transponders=general/radar-transponders
mkdir -p ${pdf_dir}/${radar_transponders}
${makepdf} --pdf-title "Radar Transponders" ${html_dir}/${radar_transponders}/index.html -o ${pdf_dir}/${radar_transponders}/index.pdf

radio_telephony_procedures=general/radio-telephony-procedures
mkdir -p ${pdf_dir}/${radio_telephony_procedures}
${makepdf} --pdf-title "Radio Telephone Procedures" ${html_dir}/${radio_telephony_procedures}/index.html -o ${pdf_dir}/${radio_telephony_procedures}/index.pdf

rules_for_prevention_of_collision=general/rules-for-prevention-of-collision
mkdir -p ${pdf_dir}/${rules_for_prevention_of_collision}
${makepdf} --pdf-title "Rules for Prevention of Collision" ${html_dir}/${rules_for_prevention_of_collision}/index.html -o ${pdf_dir}/${rules_for_prevention_of_collision}/index.pdf

rules_of_the_air=general/rules-of-the-air
mkdir -p ${pdf_dir}/${rules_of_the_air}
${makepdf} --pdf-title "Rules of the Air" index.html -o ${pdf_dir}/${rules_of_the_air}/index.pdf

the_rules_structure=general/the-rules-structure
mkdir -p ${pdf_dir}/${the_rules_structure}
${makepdf} --pdf-title "The Rules Structure" ${html_dir}/${rules_of_the_air}/index.html -o ${pdf_dir}/${the_rules_structurels}/index.pdf

# pre-flight-planning

designated_remote_areas=pre-flight-planning/designated-remote-areas
mkdir -p ${pdf_dir}/${designated_remote_areas}
${makepdf} --pdf-title "Designated Remote Areas" ${html_dir}/${designated_remote_areas}/index.html -o ${pdf_dir}/${designated_remote_areas}/index.pdf

flight_information_service=pre-flight-planning/flight-information-service
mkdir -p ${pdf_dir}/${flight_information_service}
${makepdf} --pdf-title "Flight Information Service" ${html_dir}/${flight_information_service}/index.html -o ${pdf_dir}/${flight_information_service}/index.pdf

flights_over_water=pre-flight-planning/flights-over-water
mkdir -p ${pdf_dir}/${flights_over_water}
${makepdf} --pdf-title "Flights Over Water" ${html_dir}/${flights_over_water}/index.html -o ${pdf_dir}/${flights_over_water}/index.pdf

meteorology=pre-flight-planning/meteorology
mkdir -p ${pdf_dir}/${meteorology}
${makepdf} --pdf-title "Meteorology" ${html_dir}/${meteorology}/index.html -o ${pdf_dir}/${meteorology}/index.pdf

pre_flight_briefing_and_notification=pre-flight-planning/pre-flight-briefing-and-notification
mkdir -p ${pdf_dir}/${pre_flight_briefing_and_notification}
${makepdf} --pdf-title "Pre-flight Briefing and Notification" ${html_dir}/${pre_flight_briefing_and_notification}/index.html -o ${pdf_dir}/${pre_flight_briefing_and_notification}/index.pdf

preparation=pre-flight-planning/preparation
mkdir -p ${pdf_dir}/${preparation}
${makepdf} --pdf-title "Preparation" ${html_dir}/${preparation}/index.html -o ${pdf_dir}/${preparation}/index.pdf

safety_precautions=pre-flight-planning/safety-precautions
mkdir -p ${pdf_dir}/${safety_precautions}
${makepdf} --pdf-title "Safety Precautions" ${html_dir}/${safety_precautions}/index.html -o ${pdf_dir}/${safety_precautions}/index.pdf

# operations

aerial_sporting_and_recreational_activities=operations/aerial-sporting-and-recreational-activities
mkdir -p ${pdf_dir}/${aerial_sporting_and_recreational_activities}
${makepdf} --pdf-title "Aerial Sporting and Recreational Activities" ${html_dir}/${aerial_sporting_and_recreational_activities}/index.html -o ${pdf_dir}/${aerial_sporting_and_recreational_activities}/index.pdf

air_defence_identification_zone=operations/air-defence-identification-zone
mkdir -p ${pdf_dir}/${air_defence_identification_zone}
${makepdf} --pdf-title "Air Defence Identification Zone" ${html_dir}/${air_defence_identification_zone}/index.html -o ${pdf_dir}/${air_defence_identification_zone}/index.pdf

class_d_airspace=operations/class-d-airspace
mkdir -p ${pdf_dir}/${class_d_airspace}
${makepdf} --pdf-title "Class D Airspace" ${html_dir}/${class_d_airspace}/index.html -o ${pdf_dir}/${class_d_airspace}/index.pdf

class_e_and_class_g_airspace=operations/class-e-and-class-g-airspace
mkdir -p ${pdf_dir}/${class_e_and_class_g_airspace}
${makepdf} --pdf-title "Class E and Class G Airspace" ${html_dir}/${class_e_and_class_g_airspace}/index.html -o ${pdf_dir}/${class_e_and_class_g_airspace}/index.pdf

class_g_communications=operations/class-g-communications
mkdir -p ${pdf_dir}/${class_g_communications}
${makepdf} --pdf-title "Class G Communications" ${html_dir}/${class_g_communications}/index.html -o ${pdf_dir}/${class_g_communications}/index.pdf

controlled_airspace=operations/controlled-airspace
mkdir -p ${pdf_dir}/${controlled_airspace}
${makepdf} --pdf-title "Controlled Airspace" ${html_dir}/${controlled_airspace}/index.html -o ${pdf_dir}/${controlled_airspace}/index.pdf

cruising_level_requirements=operations/cruising-level-requirements
mkdir -p ${pdf_dir}/${cruising_level_requirements}
${makepdf} --pdf-title "Cruising Level Requirements" ${html_dir}/${cruising_level_requirements}/index.html -o ${pdf_dir}/${cruising_level_requirements}/index.pdf

general_information=operations/general-information
mkdir -p ${pdf_dir}/${general_information}
${makepdf} --pdf-title "General Information" ${html_dir}/${general_information}/index.html -o ${pdf_dir}/${general_information}/index.pdf

night_vfr=operations
mkdir -p ${pdf_dir}/${night_vfr}
${makepdf} --pdf-title "General Information" ${html_dir}/${night_vfr}/night-vfr.html -o ${pdf_dir}/${night_vfr}/night-vfr.pdf

non_controlled_aerodromes=operations/non-controlled-aerodromes
mkdir -p ${pdf_dir}/${non_controlled_aerodromes}
${makepdf} --pdf-title "Non-controlled Aerodromes" ${html_dir}/${non_controlled_aerodromes}/index.html -o ${pdf_dir}/${non_controlled_aerodromes}/index.pdf

# helicopter-operations

procedures=helicopter-operations/procedures
mkdir -p ${pdf_dir}/${procedures}
${makepdf} --pdf-title "Procedures" ${html_dir}/${procedures}/index.html -o ${pdf_dir}/${procedures}/index.pdf

requirements=helicopter-operations/requirements
mkdir -p ${pdf_dir}/${requirements}
${makepdf} --pdf-title "Requirements" ${html_dir}/${requirements}/index.html -o ${pdf_dir}/${requirements}/index.pdf

# emergency-procedures

communication_failure=emergency-procedures/communication-failure
mkdir -p ${pdf_dir}/${communication_failure}
${makepdf} --pdf-title "Communication Failure" ${html_dir}/${communication_failure}/index.html -o ${pdf_dir}/${communication_failure}/index.pdf

distress_beacons=emergency-procedures/distress-beacons
mkdir -p ${pdf_dir}/${distress_beacons}
${makepdf} --pdf-title "Distress Beacons" ${html_dir}/${distress_beacons}/index.html -o ${pdf_dir}/${distress_beacons}/index.pdf

emergency_signals=emergency-procedures/emergency-signals
mkdir -p ${pdf_dir}/${emergency_signals}
${makepdf} --pdf-title "Emergency Signals" ${html_dir}/${emergency_signals}/index.html -o ${pdf_dir}/${emergency_signals}/index.pdf

forced_landings=emergency-procedures/forced-landings
mkdir -p ${pdf_dir}/${forced_landings}
${makepdf} --pdf-title "Forced Landings" ${html_dir}/${forced_landings}/index.html -o ${pdf_dir}/${forced_landings}/index.pdf

mercy_flights=emergency-procedures/mercy-flights
mkdir -p ${pdf_dir}/${mercy_flights}
${makepdf} --pdf-title "Mercy Flights" ${html_dir}/${mercy_flights}/index.html -o ${pdf_dir}/${mercy_flights}/index.pdf

planning=emergency-procedures/planning
mkdir -p ${pdf_dir}/${planning}
${makepdf} --pdf-title "Planning" ${html_dir}/${planning}/index.html -o ${pdf_dir}/${planning}/index.pdf

quick_reference=emergency-procedures
mkdir -p ${pdf_dir}/${quick_reference}
${makepdf} --pdf-title "Quick References" ${html_dir}/${quick_reference}/quick-reference.html -o ${pdf_dir}/${quick_reference}/quick-reference.pdf

# resources

abbreviations_and_acronyms=resources/abbreviations-and-acronyms
mkdir -p ${pdf_dir}/${abbreviations_and_acronyms}
${makepdf} --pdf-title "Abbreviations and Acronyms" ${html_dir}/${abbreviations_and_acronyms}/index.html -o ${pdf_dir}/${abbreviations_and_acronyms}/index.pdf

definitions=resources/definitions
mkdir -p ${pdf_dir}/${definitions}
${makepdf} --pdf-title "Definitions" ${html_dir}/${definitions}/index.html -o ${pdf_dir}/${definitions}/index.pdf

images_1=resources/images/1
mkdir -p ${pdf_dir}/${images_1}
${makepdf} --pdf-title "Images 1" ${html_dir}/${images_1}/index.html -o ${pdf_dir}/${images_1}/index.pdf

images_2=resources/images/2
mkdir -p ${pdf_dir}/${images_2}
${makepdf} --pdf-title "Images 2" ${html_dir}/${images_2}/index.html -o ${pdf_dir}/${images_2}/index.pdf

images_3=resources/images/3
mkdir -p ${pdf_dir}/${images_3}
${makepdf} --pdf-title "Images 3" ${html_dir}/${images_3}/index.html -o ${pdf_dir}/${images_3}/index.pdf

images_4=resources/images/4
mkdir -p ${pdf_dir}/${images_4}
${makepdf} --pdf-title "Images 4" ${html_dir}/${images_4}/index.html -o ${pdf_dir}/${images_4}/index.pdf

images_5=resources/images/5
mkdir -p ${pdf_dir}/${images_5}
${makepdf} --pdf-title "Images 5" ${html_dir}/${images_5}/index.html -o ${pdf_dir}/${images_5}/index.pdf

images_6=resources/images/6
mkdir -p ${pdf_dir}/${images_6}
${makepdf} --pdf-title "Images 6" ${html_dir}/${images_6}/index.html -o ${pdf_dir}/${images_6}/index.pdf

images_7=resources/images/7
mkdir -p ${pdf_dir}/${images_7}
${makepdf} --pdf-title "Images 7" ${html_dir}/${images_7}/index.html -o ${pdf_dir}/${images_7}/index.pdf

images_8=resources/images/8
mkdir -p ${pdf_dir}/${images_8}
${makepdf} --pdf-title "Images 8" ${html_dir}/${images_8}/index.html -o ${pdf_dir}/${images_8}/index.pdf

section_general_1=resources/section-general/1
mkdir -p ${pdf_dir}/${section_general_1}
${makepdf} --pdf-title "Section (general) 1" ${html_dir}/${section_general_1}/index.html -o ${pdf_dir}/${section_general_1}/index.pdf

section_general_2=resources/section-general/2
mkdir -p ${pdf_dir}/${section_general_2}
${makepdf} --pdf-title "Section (general) 2" ${html_dir}/${section_general_2}/index.html -o ${pdf_dir}/${section_general_2}/index.pdf

section_operations_1=resources/section-operations/1
mkdir -p ${pdf_dir}/${section_operations_1}
${makepdf} --pdf-title "Section (operations) 1" ${html_dir}/${section_operations_1}/index.html -o ${pdf_dir}/${section_operations_1}/index.pdf

section_operations_2=resources/section-operations/2
mkdir -p ${pdf_dir}/${section_operations_2}
${makepdf} --pdf-title "Section (operations) 2" ${html_dir}/${section_operations_2}/index.html -o ${pdf_dir}/${section_operations_2}/index.pdf

section_operations_3=resources/section-operations/3
mkdir -p ${pdf_dir}/${section_operations_3}
${makepdf} --pdf-title "Section (operations) 3" ${html_dir}/${section_operations_3}/index.html -o ${pdf_dir}/${section_operations_3}/index.pdf

section_operations_4=resources/section-operations/4
mkdir -p ${pdf_dir}/${section_operations_4}
${makepdf} --pdf-title "Section (operations) 4" ${html_dir}/${section_operations_4}/index.html -o ${pdf_dir}/${section_operations_4}/index.pdf

section_operations_5=resources/section-operations/5
mkdir -p ${pdf_dir}/${section_operations_5}
${makepdf} --pdf-title "Section (operations) 5" ${html_dir}/${section_operations_5}/index.html -o ${pdf_dir}/${section_operations_5}/index.pdf

quick_reference=resources/quick-reference
mkdir -p ${pdf_dir}/${quick_reference}
${makepdf} --pdf-title "Quick Reference" ${html_dir}/${quick_reference}/index.html -o ${pdf_dir}/${quick_reference}/index.pdf

section_emergency=resources/section-emergency
mkdir -p ${pdf_dir}/${section_emergency}
${makepdf} --pdf-title "Section (emergency)" ${html_dir}/${section_emergency}/index.html -o ${pdf_dir}/${section_emergency}/index.pdf

section_helicopter=resources/section-helicopter
mkdir -p ${pdf_dir}/${section_helicopter}
${makepdf} --pdf-title "Section (helicopter)" ${html_dir}/${section_helicopter}/index.html -o ${pdf_dir}/${section_helicopter}/index.pdf

section_planning_1=resources/section-planning/1
mkdir -p ${pdf_dir}/${section_planning_1}
${makepdf} --pdf-title "Section (planning) 1" ${html_dir}/${section_planning_1}/index.html -o ${pdf_dir}/${section_planning_1}/index.pdf

section_planning_2=resources/section-planning/2
mkdir -p ${pdf_dir}/${section_planning_2}
${makepdf} --pdf-title "Section (planning) 2" ${html_dir}/${section_planning_2}/index.html -o ${pdf_dir}/${section_planning_2}/index.pdf

section_planning_3=resources/section-planning/3
mkdir -p ${pdf_dir}/${section_planning_3}
${makepdf} --pdf-title "Section (planning) 3" ${html_dir}/${section_planning_3}/index.html -o ${pdf_dir}/${section_planning_3}/index.pdf

videos=resources/videos
mkdir -p ${pdf_dir}/${videos}
${makepdf} --pdf-title "Videos" ${html_dir}/${videos}/index.html -o ${pdf_dir}/${videos}/index.pdf
